from django.db import models

# Create your models here.

class Contenido(models.Model):
    clave = models.CharField(max_length=64) #Campo de caracteres de max 64 bites, limitandolo se aceleran las cosas
    valor = models.TextField() #
    def __str__(self):
        return f"{str(self.id)}: {self.clave}"
class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=128) #Campo de caracteres de max 64 bites, limitandolo se aceleran las cosas
    comentario = models.TextField()
    fecha = models.DateTimeField()

    def __str__(self):
        return f"{str(self.id)}: {self.titulo} ~~~{self.contenido.clave}"
    def verificar(self):
        return("cuerpo" in self.comentario)
