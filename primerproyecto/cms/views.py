from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

FORM = """
    <hr>
    <form action="" method="post">
      <div>
        <label>Valor del recurso: </label>
        <input type="text" name="valor" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""
@csrf_exempt
def get_resources(request,recurso):
    if request.method=="PUT":
      try:
        val = request.body.decode()
        contenido = Contenido.objects.get(clave=recurso)
        contenido.valor = val
        contenido.save()
      except Contenido.DoesNotExist:
        c = Contenido(clave=recurso, valor=val)
        c.save()
        
    if request.method=="POST":
      try:
        val = request.POST['valor']
        c = Contenido.objects.get(clave=recurso)
      except Contenido.DoesNotExist:
        c = Contenido(clave=recurso, valor=val)
        c.save()
    # Metodos get
    try:
        contenido = Contenido.objects.get(clave=recurso)
        html = '<html><body>' \
               f'<div>El contenido del recurso "{contenido.clave}" es : {contenido.valor}</div>' \
               f'<div>{FORM}</div>' \
               '</body></html>'
    except Contenido.DoesNotExist:
        html = '<html><body>' \
               f'<div>El recurso pedido no es existe, pero puedes crearlo!</div>' \
               f'<div>{FORM}</div>' \
               '</body></html>'
    return HttpResponse(html)
